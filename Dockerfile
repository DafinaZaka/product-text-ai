FROM node:18-alpine AS builder
COPY . .
RUN npm i -ci
RUN npm run build

FROM node:18-alpine AS runner
WORKDIR /app
COPY --from=builder /package.json package.json
COPY --from=builder /lib/ lib/
COPY --from=builder /node_modules/ node_modules

ENTRYPOINT ["node", "lib/index.js"]
EXPOSE 3000